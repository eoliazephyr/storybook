import { Component, Input } from '@angular/core';

@Component({
  selector: 'ui-eolia-zephyr',
  templateUrl: 'eolia-zephyr.component.html',
  styleUrls: [
    './eolia-zephyr.component.scss',
  ]
})
export class EoliaZephyrComponent {

  /**
   * Contenu du bouton
   */
  @Input() label = 'Button';

  /**
   * Type du bouton
   */
  @Input() type: 'basic' | 'primary' | 'accent' | 'warn' | 'disabled' | 'link' = 'basic';

}
