import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { BrowserModule } from '@angular/platform-browser';
import { EoliaZephyrComponent } from './eolia-zephyr.component';

@NgModule({
  declarations: [
    EoliaZephyrComponent
  ],
  imports: [
    MatButtonModule,
    CommonModule,
    BrowserModule
  ],
  exports: [
    EoliaZephyrComponent
  ]
})
export class EoliaZephyrModule { }
