import { TestBed } from '@angular/core/testing';

import { EoliaZephyrService } from './eolia-zephyr.service';

describe('EoliaZephyrService', () => {
  let service: EoliaZephyrService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EoliaZephyrService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
