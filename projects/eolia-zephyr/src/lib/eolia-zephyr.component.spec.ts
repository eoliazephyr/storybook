import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EoliaZephyrComponent } from './eolia-zephyr.component';

describe('EoliaZephyrComponent', () => {
  let component: EoliaZephyrComponent;
  let fixture: ComponentFixture<EoliaZephyrComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EoliaZephyrComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EoliaZephyrComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
