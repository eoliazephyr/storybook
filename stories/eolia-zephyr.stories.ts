import { Meta, moduleMetadata, Story } from "@storybook/angular";
import { EoliaZephyrComponent } from "projects/eolia-zephyr/src/public-api";
import { MatButtonModule } from '@angular/material/button';

export default {
  title: 'Eolia-Zephyr/Button',
  component: EoliaZephyrComponent,
  decorators: [
    moduleMetadata({
      imports: [MatButtonModule],
    })
  ]
} as Meta;

const Template: Story<EoliaZephyrComponent> = (args: EoliaZephyrComponent) => ({
  props: args,
});

export const Basic = Template.bind({});
Basic.args = {
//  type: 'basic',
  label: 'Button'
};

export const Primary = Template.bind({});
Primary.args = {
  type: 'primary',
  label: 'Button'
};

export const Accent = Template.bind({});
Accent.args = {
  type: 'accent',
  label: 'Button'
};

export const Warn = Template.bind({});
Warn.args = {
  type: 'warn',
  label: 'Button'
};

export const Disabled = Template.bind({});
Disabled.args = {
  type: 'disabled',
  label: 'Button Secondary'
};

export const Link = Template.bind({});
Link.args = {
  type: 'link',
  label: 'Button',
};